# Python imports
from math import *

# OpenGL imports for python
try:
    from OpenGL.GL import *
    from OpenGL.GLU import *
    from OpenGL.GLUT import *
except:
    print("OpenGL wrapper for python not found")

# Last time when sphere was re-displayed
last_time = 0

angle = 0.0
posX = 0
z = -6
shift = 1

radius = 2
verticalSlices = 20
horizontalSlices = 20
verticalAngle = 6.28 / verticalSlices
horixontalAngle = 6.28/ horizontalSlices
points = [[[] for i in range(verticalSlices)] for j in range(horizontalSlices)]
for i in range(verticalSlices):
    y = radius * cos(i * verticalAngle)
    subRadius = radius * sin(i * verticalAngle)
    for j in range(horizontalSlices):
        sub = [subRadius * cos(j * horixontalAngle), y, subRadius * sin(j * horixontalAngle)]
        points[i][j] = sub
# The sphere class
# class Sphere:
#
#     # Constructor for the sphere class
#     def __init__(self, radius):
#
#         # Radius of sphere
#         self.radius = radius
#
#         # Number of latitudes in sphere
#         self.lats = 20
#
#         # Number of longitudes in sphere
#         self.longs = 20
#
#         self.user_theta = 0
#         self.user_height = 0
#
#         # Direction of light
#         self.direction = [1.0, 2.0, 4.0, 1.0]
#
#         # Intensity of light
#         self.intensity = [1, 0.7, 0.7, 1.0]
#
#         # Intensity of ambient light
#         self.ambient_intensity = [0, 0.3, 0.3, 1.0]
#
#         # The surface type(Flat or Smooth)
#         self.surface = GL_FLAT
#
#     # Initialize
#     def init(self):
#
#         # Set background color to black
#         glClearColor(0.0, 0.0, 0.0, 0.0)
#
#         self.compute_location()
#
#         # Set OpenGL parameters
#         glEnable(GL_DEPTH_TEST)
#
#         # Enable lighting
#         glEnable(GL_LIGHTING)
#
#         # Set light model
#         glLightModelfv(GL_LIGHT_MODEL_AMBIENT, self.ambient_intensity)
#
#         # Enable light number 0
#         glEnable(GL_LIGHT0)
#
#         # Set position and intensity of light
#         glLightfv(GL_LIGHT0, GL_POSITION, self.direction)
#         glLightfv(GL_LIGHT0, GL_DIFFUSE, self.intensity)
#
#         # Setup the material
#         glEnable(GL_COLOR_MATERIAL)
#         glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE)
#
#     # Compute location
#     def compute_location(self):
#         x = 2 * cos(self.user_theta)
#         y = 2 * sin(self.user_theta)
#         z = self.user_height
#         d = sqrt(x * x + y * y + z * z)
#
#         # Set matrix mode
#         glMatrixMode(GL_PROJECTION)
#
#         # Reset matrix
#         glLoadIdentity()
#         glFrustum(-d * 0.5, d * 0.5, -d * 0.5, d * 0.5, d - 1.1, d + 1.1)
#
#         # Set camera
#         gluLookAt(x, y, z, 0, 0, 0, 0, 0, 1)
#
#     # Display the sphere
#     def display(self):
#         glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
#
#         # Set color to white
#         glColor3f(1.0, 1.0, 1.0)
#
#         # Set shade model
#         glShadeModel(self.surface)
#
#         self.draw()
#         glutSwapBuffers()
#
#     # Draw the sphere
#     def draw(self):
#         for i in range(0, self.lats + 1):
#             lat0 = pi * (-0.5 + float(float(i - 1) / float(self.lats)))
#             z0 = sin(lat0)
#             zr0 = cos(lat0)
#
#             lat1 = pi * (-0.5 + float(float(i) / float(self.lats)))
#             z1 = sin(lat1)
#             zr1 = cos(lat1)
#
#             # Use Quad strips to draw the sphere
#             glBegin(GL_LINES)
#
#             for j in range(0, self.longs + 1):
#                 lng = 2 * pi * float(float(j - 1) / float(self.longs))
#                 x = cos(lng)
#                 y = sin(lng)
#                 glNormal3f(x * zr0, y * zr0, z0)
#                 glVertex3f(x * zr0, y * zr0, z0)
#                 glNormal3f(x * zr1, y * zr1, z1)
#                 glVertex3f(x * zr1, y * zr1, z1)
#
#             glEnd()
#
#     # Keyboard controller for sphere
#     def special(self, key, x, y):
#
#         # Scale the sphere up or down
#         if key == GLUT_KEY_UP:
#             self.user_height += 0.1
#         if key == GLUT_KEY_DOWN:
#             self.user_height -= 0.1
#
#         # Rotate the cube
#         if key == GLUT_KEY_LEFT:
#             self.user_theta += 0.1
#         if key == GLUT_KEY_RIGHT:
#             self.user_theta -= 0.1
#
#         # Toggle the surface
#         if key == GLUT_KEY_F1:
#             if self.surface == GL_FLAT:
#                 self.surface = GL_SMOOTH
#             else:
#                 self.surface = GL_FLAT
#
#         self.compute_location()
#         glutPostRedisplay()
#
#     # The idle callback
#     def idle(self):
#         global last_time
#         time = glutGet(GLUT_ELAPSED_TIME)
#
#         if last_time == 0 or time >= last_time + 40:
#             last_time = time
#             glutPostRedisplay()
#
#     # The visibility callback
#     def visible(self, vis):
#         if vis == GLUT_VISIBLE:
#             glutIdleFunc(self.idle)
#         else:
#             glutIdleFunc(None)

def DrawSphere():
    global points
    for i in range(verticalSlices):
        glBegin(GL_LINE_STRIP)
        for j in range(horizontalSlices):
            glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
        glEnd()
    for j in range(horizontalSlices):
        if not j % 3:
            glBegin(GL_LINE_STRIP)
            for i in range(verticalSlices):
                glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
            glEnd()

def DrawSphere1():
    global points
    for i in range(verticalSlices):
        glBegin(GL_LINE_STRIP)
        for j in range(horizontalSlices):
            glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
        glEnd()
    for j in range(horizontalSlices):
        if j % 3:
            glBegin(GL_LINE_STRIP)
            for i in range(verticalSlices):
                glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
            glEnd()


def w(rad):
    global z, posX
    z += shift


def s(rad):
    global z, posX
    z -= shift

def a(rad):
    global z, posX
    posX += shift



def d(rad):
    global z, posX
    posX -= shift



def q(rad):
    global angle
    angle -=1

def e(rad):
    global angle
    angle +=1


func_map ={
    b'a':a,
    b'w':w,
    b's':s,
    b'd':d,
    b'q':q,
    b'e':e

}
def OnKeyDown(key,  x,  y):
    print(key)
    rad = angle*3.14/180
    func_map.get(key, lambda x: print(x))(rad)
    glutPostRedisplay()


def display():
    glClearColor(0, 0, 0, 0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    width = glutGet(GLUT_WINDOW_WIDTH)
    height = glutGet(GLUT_WINDOW_HEIGHT)
    min = height if height < width  else width
    glFrustum(-width / min, width / min, -height / min, height / min, 6.0, 10000.0)

    glMatrixMode(GL_MODELVIEW)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LESS)
    glLoadIdentity()
    glTranslatef(0, 0, -20)
    glRotatef(angle, 0, 1, 0)
    glTranslatef(posX, 0, z + 6)
    print(angle, posX, z)
    glColor3d(0,1,1)
    DrawSphere()
    glColor3d(1, 1, 0)
    DrawSphere1()
    glutSwapBuffers()


def reshape(w, h):
    glViewport(0, 0, w, h)
    glutPostRedisplay()



def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE)
    glutInitWindowSize(400, 400)
    mainWindow = glutCreateWindow("Cubes")
    glutSetWindow(mainWindow)
    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutKeyboardFunc(OnKeyDown)
    # glewInit()
    glutMainLoop()




# # The main function
# def main():
#
#     # Initialize the OpenGL pipeline
#     glutInit(sys.argv)
#
#     # Set OpenGL display mode
#     glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
#
#     # Set the Window size and position
#     glutInitWindowSize(300, 300)
#     glutInitWindowPosition(50, 100)
#
#     # Create the window with given title
#     glutCreateWindow('Sphere')
#
#     # Instantiate the sphere object
#     s = Sphere(5.0)
#
#     s.init()
#
#     # Set the callback function for display
#     glutDisplayFunc(s.display)
#
#     # Set the callback function for the visibility
#     glutVisibilityFunc(s.visible)
#
#     # Set the callback for special function
#     glutSpecialFunc(s.special)
#
#     # Run the OpenGL main loop
#     glutMainLoop()


# Call the main function
if __name__ == '__main__':
    main()


