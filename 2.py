from math import *
from random import randint

# OpenGL imports for python
try:
    from OpenGL.GL import *
    from OpenGL.GLU import *
    from OpenGL.GLUT import *
except:
    print("OpenGL wrapper for python not found")

direction = {
    'Forward': False,
    'Backward' : False,
    'Left': False,
    'Right': False,
    'TurnLeft': False,
    'TurnRight' : False,
    'TurnUp': False,
    'TurnDown' : False,
    'Up':False,
    'Down':False,
}

angle = 0.0
angle_shift = 2
PI_180 = acos(-1.0) / 180.0
viewCoords={}
cubes = []
speed = []
radius = 500
shift = 1
verticalSlices = 120
horizontalSlices = 120
verticalAngle = acos(-1.0)*2 / (verticalSlices)
horixontalAngle = acos(-1.0)*2 / (horizontalSlices)
points = [[[] for i in range(verticalSlices)] for j in range(horizontalSlices)]
for i in range(verticalSlices):
    y = radius * cos(i * verticalAngle)
    subRadius = radius * sin(i * verticalAngle)
    for j in range(horizontalSlices):
        sub = [subRadius * cos(j * horixontalAngle), y, subRadius * sin(j * horixontalAngle)]
        points[j][i] = sub


colors =[
    [1, 0, 1],
    [0, 0, 1],
    [1, 0, 0],
    [0, 1, 1],
    [1, 1, 0],
    [0, 1, 0],
]


def SetFrustum():
    h = glutGet(GLUT_WINDOW_HEIGHT)
    w = glutGet(GLUT_WINDOW_WIDTH)
    if w < h:
        glFrustum(-1.0, 1.0, -1.0 * h / w, 1.0 * h / w, 5.0, 5000.0)
    else:
        glFrustum(-1.0 * w / h, 1.0 * w / h, -1.0, 1.0, 5.0, 5000.0)


def InitViewCoords():
    global viewCoords
    viewCoords["angle"] = 180
    viewCoords["angle_y"] = 0
    viewCoords["size"] = 0
    viewCoords["axis"]={
        "x":0,
        "y":1,
        "z":0,
    }
    # viewCoords["position"] ={
    #     "x": 0,
    #     "y": 0,
    #     "z": -10,
    # }
    print( points[int(horizontalSlices/4)][int(verticalSlices/4)])
    point = points[int(horizontalSlices/4)][int(verticalSlices/4)]
    viewCoords["position"] = {
        "x": point[0],
        "y":  point[1],
        "z":  point[2],
    }


func_map ={
    b'a':'Left',
    b'w':'Forward',
    b's':'Backward',
    b'd':'Right',
    b'q':'TurnLeft',
    b'e':'TurnRight',
    b'r':'Up',
    b'f':'Down',
    b't':'TurnUp',
    b'g':'TurnDown',
}



def OnKeyDown(key,x,y):
    global direction
    if key in func_map:
        direction[func_map[key]] = True


def OnKeyUp(key,x,y):
    global direction
    if key in func_map:
        direction[func_map[key]] = False


def DrawSphere():
    global points
    for i in range(verticalSlices):
        if not i % 3:
        #if True:
            glBegin(GL_LINE_STRIP)
            for j in range(horizontalSlices):
                glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
            glEnd()
    for j in range(horizontalSlices):
        glBegin(GL_LINE_STRIP)
        for i in range(verticalSlices):
            glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
        glEnd()

def DrawSphere1():
    global points
    for i in range(verticalSlices):
        if i % 3 == 1:
            # if True:
            glBegin(GL_LINE_STRIP)
            for j in range(horizontalSlices):
                glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
            glEnd()

def DrawSphere2():
    global points
    for i in range(verticalSlices):
        if i % 3 == 2:
            # if True:
            glBegin(GL_LINE_STRIP)
            for j in range(horizontalSlices):
                glVertex3d(points[i][j][0], points[i][j][1], points[i][j][2])
            glEnd()

def MoveCamera(value):
    global direction, viewCoords, angle_shift, shift
    viewAngle = viewCoords['angle'] * PI_180
    if direction["Forward"]:
        viewCoords["position"]["z"] += shift * cos(viewAngle)
        viewCoords["position"]["x"] -= shift * sin(viewAngle)

    if direction["Backward"]:
        viewCoords["position"]["z"] -= shift * cos(viewAngle)
        viewCoords["position"]["x"] += shift * sin(viewAngle)

    if direction["Left"]:
        viewCoords["position"]["z"] += shift * sin(viewAngle)
        viewCoords["position"]["x"] += shift * cos(viewAngle)

    if direction["Right"]:
        viewCoords["position"]["z"] -= shift * sin(viewAngle)
        viewCoords["position"]["x"] -= shift * cos(viewAngle)

    if direction["Up"]:
        viewCoords["position"]["y"] -= shift
    if direction["Down"]:
        viewCoords["position"]["y"] += shift

    if direction["TurnUp"]:
        viewCoords["angle_y"] -= angle_shift
    if direction["TurnDown"]:
        viewCoords["angle_y"] += angle_shift

    if direction["TurnLeft"]:
        viewCoords["angle"] -= angle_shift

    if direction["TurnRight"]:
        viewCoords["angle"] += angle_shift
    glutTimerFunc(10, MoveCamera, 0)
    glutPostRedisplay()


def display():
    global viewCoords
    glClearColor(0, 0, 0, 0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    SetFrustum()
    glMatrixMode(GL_MODELVIEW)
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LESS)
    glLoadIdentity()
    glTranslatef(0, 0, 0)
    #glRotatef(viewCoords["angle"], viewCoords["axis"]["x"], viewCoords["axis"]["y"], viewCoords["axis"]["z"])
    # print(viewCoords["angle"])
    glRotatef(viewCoords["angle"], 0, 1, 0)
    viewAngle = viewCoords['angle'] * PI_180
    glRotatef(viewCoords["angle_y"], 1* cos(viewAngle), 0, 1* sin(viewAngle))
    glTranslatef(viewCoords["position"]["x"], viewCoords["position"]["y"], viewCoords["position"]["z"])
    glPushMatrix()
    glTranslatef(0, 0, 0)
    glColor3d(0, 1, 1)
    DrawSphere()
    glColor3d(1, 1, 0)
    DrawSphere1()
    glColor3d(1, 0, 1)
    DrawSphere2()
    glPopMatrix()
    glutSwapBuffers()



def reshape(w, h):
    glViewport(0, 0, w, h)


def main(count, args):
    glutInit(count, args)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE)
    glutInitWindowSize(400, 400)
    mainWindow = glutCreateWindow("Some Title")
    glutSetWindow(mainWindow)
    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutKeyboardFunc(OnKeyDown)
    glutKeyboardUpFunc(OnKeyUp)
    InitViewCoords()
    glutTimerFunc(10, MoveCamera, 0)
    glutMainLoop()

main(1,[])